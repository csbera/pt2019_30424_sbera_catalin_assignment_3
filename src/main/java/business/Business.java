package business;
/**
 * Interface that is used for representing the basic operations of the implementing methods
 * 
 * @author catal
 *
 */
public interface Business {

  String[] getFields();

  String[][] getData();
}
