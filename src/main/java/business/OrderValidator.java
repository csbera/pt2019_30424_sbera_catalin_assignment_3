package business;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import dao.GenericDAO;
import model.*;

public class OrderValidator {

  private List<Orders> orders;
  private List<Product> products;
  private List<Client> clients;
  private List<Stock> stocks;
  private GenericDAO<Product> genericProduct;
  private GenericDAO<Stock> genericStock;
  private GenericDAO<Client> genericClient;
  private GenericDAO<Orders> genericOrder;

  public OrderValidator() {
    this.genericProduct = new GenericDAO<Product>(Product.class);
    this.genericStock = new GenericDAO<Stock>(Stock.class);
    this.genericClient = new GenericDAO<Client>(Client.class);
    this.genericOrder = new GenericDAO<Orders>(Orders.class);
    this.products = genericProduct.getAll();
    this.stocks = genericStock.getAll();
    this.clients = genericClient.getAll();
    this.orders = genericOrder.getAll();
  }

  /**
   * Determines if a new Stock can be created using the given values
   * 
   * @param values - String objects of the possible parameters of a Stock class
   * @return int - (-1) for invalid format, (1) for valid existing Stock, (0) for valid non-existing Stock
   */
  public int getStatus(String[] values) {
    update();
    if (values.length != 4)
      return -1;
    int id, idClient, idProduct, ammount;
    try {
      id = Integer.parseInt(values[0]);
      idClient = Integer.parseInt(values[1]);
      idProduct = Integer.parseInt(values[2]);
      ammount = Integer.parseInt(values[3]);
    }
    catch (NumberFormatException e) {
      return -1;
    }
    int remainingStock = 0;

    if (id <= 0)
      return -1;
    int ok = 0;
    for (Product p : products) {
      if (p.getId() == idProduct) {
        remainingStock = 1;
        break;
      }
    }
    for (Client c : clients) {
      if (c.getId() == idClient) {
        ok = 1;
        break;
      }
    }
    if (ok == 0 || remainingStock == 0) {
      return -1;
    }

    for (Orders o : orders) {
      if (o.getId() == id) {
        return 1;
      }
    }
    return enoughStock(ammount, idProduct);
  }

  /**
   * Determines if there is enough stock for creating a new order
   * 
   * @param ammount - amount of products to add to the new order
   * @param id - id of the given product
   * @return int - (0) if a valid Order can be created, (-2) if the is not enough stock
   */
  private int enoughStock(int ammount, int id) {
    for (Stock s : stocks) {
      if (s.getId() == id) {
        if (s.getAmount() - ammount < 0)
          return -2;
        break;
      }
    }
    return 0;
  }

  /**
   * Updates the List of objects of the class
   */
  public void update() {
    this.products = genericProduct.getAll();
    this.stocks = genericStock.getAll();
    this.clients = genericClient.getAll();
    this.orders = genericOrder.getAll();
  }

  /**
   * Inserts a new Order into the SQL Database and creates a ".txt" file with the order
   * 
   * @param parameters
   */
  public void insert(String[] parameters) {
    Orders order = new Orders(parameters[0], parameters[1], parameters[2], parameters[3]);
    Client client = genericClient.getWithId(Integer.parseInt(parameters[1]));
    Product product = genericProduct.getWithId(Integer.parseInt(parameters[2]));
    Stock oldStock = genericStock.getWithId(Integer.parseInt(parameters[2]));
    Stock newStock = new Stock(oldStock.getId(), oldStock.getAmount() - Integer.parseInt(parameters[3]));
    genericOrder.insert(order);
    genericStock.update(newStock.getId(), newStock);
    createReciept(client, product, Integer.parseInt(parameters[3]));
  }

  /**
   * Creates a new file with a random name with Order information
   * 
   * @param client - the client that ordered
   * @param product - the product that has been ordered
   * @param amount - the amount that has been order
   */
  private void createReciept(Client client, Product product, int amount) {
    File file = new File("./src/main/resources/" + generateRecieptName() + ".txt");

    // Create the file
    try {
      if (file.createNewFile()) {
        System.out.println("Order is created!");
      }
      else {
        System.out.println("Order already exists.");
        return;
      }
    }
    catch (IOException e) {
      e.printStackTrace();
    }

    // Write Content
    FileWriter writer = null;
    try {
      writer = new FileWriter(file);
      writer.write(generateRecieptData(client, product, amount));
      writer.close();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Generates a random string of length 10
   * 
   * @return String - a random string of length 10
   */
  private String generateRecieptName() {
    int leftLimit = 97; // letter 'a'
    int rightLimit = 122; // letter 'z'
    int targetStringLength = 10;
    Random random = new Random();
    StringBuilder buffer = new StringBuilder(targetStringLength);
    for (int i = 0; i < targetStringLength; i++) {
      int randomLimitedInt = leftLimit + (int) (random.nextFloat() * (rightLimit - leftLimit + 1));
      buffer.append((char) randomLimitedInt);
    }
    return buffer.toString();
  }

  /**
   * Generates the receipt data depending on the given parameters
   * 
   * @param c - the Client that ordered
   * @param p - the Product that was ordered
   * @param a - the amount that was ordered
   * @return String - generated order info
   */
  private String generateRecieptData(Client c, Product p, int a) {
    return "Client with given name: " + c.getName() + " ordered " + a + " units of product with given name: "
        + p.getName() + " with delivery adress: " + c.getAdress();
  }

}
