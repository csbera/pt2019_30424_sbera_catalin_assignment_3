package business;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import dao.GenericDAO;

/**
 * Generates a Business object depending on the given class
 * 
 * @author catal
 *
 * @param <T> - given class
 */
public class GenericBusiness<T> implements Business {

  private String[] fields;
  private Method[] getters;
  private GenericDAO<T> generic;
  private Class referencedClass;

  public GenericBusiness(Class clas) {
    this.referencedClass = clas;
    this.generic = new GenericDAO<T>(clas);
    Field[] fields = (clas).getDeclaredFields();
    getters = getSortedGettersAfterFields(fields, clas.getMethods());
    this.fields = new String[fields.length];
    for (int i = 0; i < fields.length; i++) {
      this.fields[i] = fields[i].getName();
    }
  }

  /**
   * Returns the fields of the SQL table
   */
  public String[] getFields() {
    return fields;
  }

  /**
   * Returns the date of the SQL table
   */
  public String[][] getData() {
    List<T> stocks = generic.getAll();
    String[][] toReturn = new String[stocks.size()][];
    for (int i = 0; i < stocks.size(); i++) {
      toReturn[i] = getRow(stocks.get(i));
    }
    return toReturn;
  }

  /**
   * Returns a row of the given table depending on a T object
   * 
   * @param t - Object to be converted into a String[]
   * @return String[] - row that was generated
   */
  public String[] getRow(T t) {
    String[] toReturn = new String[fields.length];
    int nr = 0;
    for (Method m : getters) {
      try {
        if (m.invoke(t).getClass() == Integer.class) {
          toReturn[nr] = Integer.toString((Integer) m.invoke(t));
        }
        else {
          toReturn[nr] = (String) m.invoke(t);
        }
        nr++;
      }
      catch (IllegalAccessException e) {
        e.printStackTrace();
      }
      catch (IllegalArgumentException e) {
        e.printStackTrace();
      }
      catch (InvocationTargetException e) {
        e.printStackTrace();
      }
    }
    return toReturn;
  }

  /**
   * Checks if a method is a getter
   * 
   * @param method
   * @return boolean - (true) when method is a getter, (false) when method is not a getter
   */
  private boolean isGetter(Method method) {
    if (!method.getName().startsWith("get"))
      return false;
    if (method.getParameterTypes().length != 0)
      return false;
    return true;
  }

  /**
   * Gets a sorted array of getters that have corresponding locations with the fields
   * 
   * @param fields - the fields of the Class
   * @param methods - the methods of the Class
   * @return Method[] - sorted array of getters, without getClass()
   */
  private Method[] getSortedGettersAfterFields(Field[] fields, Method[] methods) {
    ArrayList<Method> getters = new ArrayList<Method>();
    for (Method m : methods) {
      if (isGetter(m) && !m.getName().equals("getClass")) {
        getters.add(m);
      }
    }
    Method[] toRetrun = new Method[getters.size()];
    for (int i = 0; i < fields.length; i++) {
      for (Method m : getters) {
        if (fields[i].getName().equals(m.getName().substring(3, m.getName().length()).toLowerCase())) {
          toRetrun[i] = m;
          break;
        }
      }
    }
    return toRetrun;
  }

}
