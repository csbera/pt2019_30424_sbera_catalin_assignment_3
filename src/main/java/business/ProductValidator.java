package business;

import java.util.List;

import dao.GenericDAO;
import model.Product;

public class ProductValidator {

  private List<Product> products;
  private GenericDAO<Product> genericProduct;

  public ProductValidator() {
    this.genericProduct = new GenericDAO<Product>(Product.class);
    this.products = genericProduct.getAll();
  }

  /**
   * Determines if a new Product can be created using the given values
   * 
   * @param values - String objects of the possible parameters of a Product class
   * @return int - (-1) for invalid format, (1) for valid existing Product, (0) for valid non-existing Product
   */
  public int getStatus(String[] values) {
    update();
    if (values.length != 2)
      return -1;
    int id = 0;
    try {
      id = Integer.parseInt(values[0]);
    }
    catch (NumberFormatException e) {
      return -1;
    }
    String name = values[1];
    if (id <= 0 || name.length() == 0)
      return -1;
    for (Product p : products) {
      if (p.getId() == id) {
        return 1;
      }
    }
    return 0;
  }

  /**
   * Updates the List of objects of the class
   */
  public void update() {
    products = genericProduct.getAll();
  }

  /**
   * Adds a new Product with given paramters to the database
   * 
   * @param pParameters
   * @return int - (0) if an SQL error occurs, (!0) if added succesfuly
   */
  public int add(String[] pParameters) {
    Product product = new Product(pParameters[0], pParameters[1]);
    return genericProduct.insert(product);
  }

  /**
   * Deletes the Product, created with given parameters, from the database.
   * 
   * @param pParameters
   * @return int - (0) if an SQL error occurs, (!0) if added succesfuly
   */
  public int delete(String[] pParameters) {
    return genericProduct.delete(Integer.parseInt(pParameters[0]));
  }

  /**
   * Updates the Product with the given id from the table
   * 
   * @param pParameters
   * @return int - (0) if an SQL error occurs, (!0) if added succesfuly
   */
  public int update(String[] pParameters) {
    Product product = new Product(pParameters[0], pParameters[1]);
    return genericProduct.update(product.getId(), product);

  }

}
