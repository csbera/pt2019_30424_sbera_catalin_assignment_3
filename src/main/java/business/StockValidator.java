package business;

import java.util.List;

import dao.GenericDAO;
import model.Product;
import model.Stock;

public class StockValidator {

  private List<Product> products;
  private List<Stock> stocks;
  private GenericDAO<Product> genericProduct;
  private GenericDAO<Stock> genericStock;

  public StockValidator() {
    this.genericProduct = new GenericDAO<Product>(Product.class);
    this.genericStock = new GenericDAO<Stock>(Stock.class);
    this.products = genericProduct.getAll();
    this.stocks = genericStock.getAll();
  }

  /**
   * Determines if a new Stock can be created using the given values
   * 
   * @param values - String objects of the possible parameters of a Stock class
   * @return int - (-1) for invalid format, (1) for valid existing Stock, (0) for valid non-existing Stock
   */
  public int getStatus(String[] values) {
    update();
    if (values.length != 2)
      return -1;
    int id = 0;
    int ammount = 0;
    try {
      id = Integer.parseInt(values[0]);
      ammount = Integer.parseInt(values[1]);
    }
    catch (NumberFormatException e) {
      return -1;
    }
    if (id <= 0 || ammount <= 0)
      return -1;
    int ok = 0;
    for (Product p : products) {
      if (p.getId() == id) {
        ok = 1;
        break;
      }
    }
    if (ok == 0) {
      return -1;
    }

    for (Stock s : stocks) {
      if (s.getId() == id) {
        return 1;
      }
    }

    return 0;
  }

  /**
   * Updates the List of objects of the class
   */
  public void update() {
    this.products = genericProduct.getAll();
    this.stocks = genericStock.getAll();
  }

  /**
   * Adds a new Stock with given paramters to the database
   * 
   * @param sParameters
   * @return int - (0) if an SQL error occurs, (!0) if added succesfuly
   */
  public int add(String[] sParameters) {
    Stock stock = new Stock(sParameters[0], sParameters[1]);
    return genericStock.insert(stock);
  }

  /**
   * Deletes a Stock with given parameters from the database
   * 
   * @param sParameters
   * @return int - (0) if an SQL error occurs, (!0) if added successfully
   */
  public int delete(String[] sParameters) {
    return genericStock.delete(Integer.parseInt(sParameters[0]));
  }

  /**
   * Updates a Stock with given paramters in the database
   * 
   * @param sParameters
   * @return int - (0) if an SQL error occurs, (!0) if added succesfuly
   */
  public int update(String[] sParameters) {
    Stock stock = new Stock(sParameters[0], sParameters[1]);
    return genericStock.update(stock.getId(), stock);
  }

}
