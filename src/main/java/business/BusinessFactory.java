package business;

import model.*;

/**
 * Factory class that returns the specific business objects depending on the request.
 * 
 * @author catal
 *
 */
public class BusinessFactory {
  /**
   * Static method that returns a requested object.
   * 
   * @param tableName - Name of the tableType to return
   * @return Business - Requested business class
   */
  public static Business getBusiness(String tableName) {
    Business toReturn = null;
    switch (tableName) {
    case "Client":
      toReturn = new GenericBusiness<Client>(Client.class);
      break;
    case "Product":
      toReturn = new GenericBusiness<Product>(Product.class);
      break;
    case "Stock":
      toReturn = new GenericBusiness<Stock>(Stock.class);
      break;
    case "Orders":
      toReturn = new GenericBusiness<Orders>(Orders.class);
      break;

    default:
      break;
    }
    return toReturn;
  }
}
