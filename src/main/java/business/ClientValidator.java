package business;

import java.util.List;

import dao.GenericDAO;
import model.Client;

/**
 * Validator for the Cient table.
 * 
 * @author catal
 *
 */
public class ClientValidator {

  private List<Client> clients;
  private GenericDAO<Client> genericClient;

  public ClientValidator() {
    genericClient = new GenericDAO<>(Client.class);
    this.clients = genericClient.getAll();
  }

  /**
   * Determines if a new Client can be created using the given values
   * 
   * @param values - String objects of the possible parameters of a Client class
   * @return int - (-1) for invalid format, (1) for valid existing client, (0) for valid non-existing client
   */
  public int getStatus(String[] values) {
    update();
    if (values.length != 3)
      return -1;
    int id = 0;
    try {
      id = Integer.parseInt(values[0]);
    }
    catch (NumberFormatException e) {
      return -1;
    }
    String name = values[1];
    String adress = values[2];
    if (id <= 0 || name.length() == 0 || adress.length() == 0)
      return -1;
    for (Client c : clients) {
      if (c.getId() == id) {
        return 1;
      }
    }
    return 0;
  }

  /**
   * Updates the List of objects of the class
   */
  public void update() {
    this.clients = genericClient.getAll();
  }

  /**
   * Adds a new Client with given paramters to the database
   * 
   * @param parameters
   * @return int - (0) if an SQL error occurs, (!0) if added succesfuly
   */
  public int add(String[] parameters) {
    Client client = new Client(parameters[0], parameters[1], parameters[2]);
    return genericClient.insert(client);
  }

  /**
   * Updates the Client, created with given parameters, from the database.
   * 
   * @param parameters
   * @return int - (0) if an SQL error occurs, (!0) if added succesfuly
   */
  public int update(String[] parameters) {
    Client client = new Client(parameters[0], parameters[1], parameters[2]);
    return genericClient.update(client.getId(), client);
  }

  /**
   * Deletes the Client with the given id from the table
   * 
   * @param parameters
   * @return int - (0) if an SQL error occurs, (!0) if added succesfuly
   */
  public int delete(String[] parameters) {
    return genericClient.delete(Integer.parseInt(parameters[0]));
  }

}
