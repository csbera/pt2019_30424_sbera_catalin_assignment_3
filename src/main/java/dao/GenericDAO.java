package dao;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;

/**
 * Generic Data access object that uses connection to establish a connection to the database
 * 
 * @author catal
 *
 * @param <T> - Class that corresponds to a table in the database
 */
public class GenericDAO<T> {

  protected static final Logger LOGGER = Logger.getLogger(GenericDAO.class.getName());
  private String findAllStatementString = "SELECT * FROM ?";
  private String findStatementString = "SELECT * FROM ? where id = ?";
  private String insertStatementString = "INSERT INTO ? (?)" + " VALUES (?)";
  private String deleteStatementString = "DELETE FROM ? WHERE id = ?";
  private String updateStatementString = "UPDATE ? SET ? WHERE id = ?";

  private Class<T> referencedClass;
  private Field[] fields;
  private Method[] methods;

  public GenericDAO(Class<T> clas) {
    referencedClass = clas;
    this.setup();
  }

  /**
   * Initializes the statements necessary to getting, updating, inserting and deleteing data from the table
   */
  private void setup() {
    Class clas = referencedClass;
    String table = clas.getSimpleName().toLowerCase();
    findAllStatementString = findAllStatementString.replaceFirst("[?]", table);
    findStatementString = findStatementString.replaceFirst("[?]", table);
    insertStatementString = insertStatementString.replaceFirst("[?]", table);
    deleteStatementString = deleteStatementString.replaceFirst("[?]", table);
    updateStatementString = updateStatementString.replaceFirst("[?]", table);

    fields = referencedClass.getDeclaredFields();
    methods = getSortedGettersAfterFields(fields, referencedClass.getMethods());
    String fieldListInsert = "";
    String valuesListInsert = "";
    String fieldListUpdate = "";
    for (Field f : fields) {
      f.setAccessible(true);
      fieldListInsert = fieldListInsert.concat(f.getName() + ",");
      valuesListInsert = valuesListInsert.concat("?,");
      fieldListUpdate = fieldListUpdate.concat(f.getName() + " = ?, ");
    }
    fieldListInsert = fieldListInsert.substring(0, fieldListInsert.length() - 1);
    valuesListInsert = valuesListInsert.substring(0, valuesListInsert.length() - 1);
    fieldListUpdate = fieldListUpdate.substring(0, fieldListUpdate.length() - 2);

    insertStatementString = insertStatementString.replaceFirst("[?]", fieldListInsert);
    insertStatementString = insertStatementString.replaceFirst("[?]", valuesListInsert);
    updateStatementString = updateStatementString.replaceFirst("[?]", fieldListUpdate);

  }

  /**
   * Gets a row from the database with a specific id
   * 
   * @param id - id of the row
   * @return T - object created with the data from the given row
   */
  public T getWithId(int id) {
    T toReturn = null;

    Connection dbConnection = ConnectionFactory.getConnection();
    PreparedStatement findStatement = null;
    ResultSet rs = null;

    Constructor<?> constructor = referencedClass.getConstructors()[1];
    Field[] fields = referencedClass.getDeclaredFields();
    Object[] parameters = new String[fields.length];

    try {
      findStatement = dbConnection.prepareStatement(findStatementString);
      findStatement.setInt(1, id);
      rs = findStatement.executeQuery();
      rs.next();
      for (int i = 0; i < fields.length; i++) {
        parameters[i] = rs.getString(fields[i].getName());
      }

      toReturn = (T) constructor.newInstance(parameters);
    }
    catch (SQLException e) {
      LOGGER.log(Level.WARNING, "Main:findById " + e.getMessage());
    }
    catch (InstantiationException e) {
    }
    catch (IllegalAccessException e) {
    }
    catch (IllegalArgumentException e) {
    }
    catch (InvocationTargetException e) {
    }
    finally {
      ConnectionFactory.close(rs);
      ConnectionFactory.close(findStatement);
      ConnectionFactory.close(dbConnection);
    }

    return toReturn;
  }

  /**
   * Gets a list of all the objects from a table
   * 
   * @return List<T> - a list with all the rows from the table converted into objects
   */
  public List<T> getAll() {
    ArrayList<T> toReturn = new ArrayList<T>();
    Connection dbConnection = ConnectionFactory.getConnection();
    PreparedStatement findStatement = null;
    ResultSet rs = null;
    Constructor<?> constructor = referencedClass.getConstructors()[1];
    Field[] fields = referencedClass.getDeclaredFields();
    Object[] parameters = new String[fields.length];
    try {
      findStatement = dbConnection.prepareStatement(findAllStatementString);
      rs = findStatement.executeQuery();
      while (rs.next()) {
        for (int i = 0; i < fields.length; i++) {
          parameters[i] = rs.getString(fields[i].getName());
        }
        toReturn.add((T) constructor.newInstance(parameters));
      }
    }
    catch (SQLException e) {
      LOGGER.log(Level.WARNING, "Main:findAll " + e.getMessage());
    }
    catch (InstantiationException e) {
    }
    catch (IllegalAccessException e) {
    }
    catch (IllegalArgumentException e) {
    }
    catch (InvocationTargetException e) {
    }
    finally {
      ConnectionFactory.close(rs);
      ConnectionFactory.close(findStatement);
      ConnectionFactory.close(dbConnection);
    }
    return toReturn;
  }

  /**
   * Inserts a new object into the table
   * 
   * @param t - object to be inserted
   * @return int - (-1) if insertion fails, (insertedId) if insertion succeeds
   */
  public int insert(T t) {
    Connection dbConnection = ConnectionFactory.getConnection();
    PreparedStatement insertStatement = null;
    int insertedId = -1;
    try {
      insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
      int insertNr = 2;
      for (Method m : methods) {
        if (isGetter(m) && !m.getName().equals("getClass")) {
          // System.out.println(m.getName());
          if (m.getName().equals("getId")) {
            insertStatement.setInt(1, (Integer) m.invoke(t));
            continue;
          }
          if (m.invoke(t).getClass() == Integer.class) {
            insertStatement.setInt(insertNr, (Integer) m.invoke(t));
          }
          else {
            insertStatement.setString(insertNr, (String) m.invoke(t));
          }
          insertNr++;
        }
      }
      insertStatement.executeUpdate();

      ResultSet rs = insertStatement.getGeneratedKeys();
      if (rs.next()) {
        insertedId = rs.getInt(1);
      }
    }
    catch (SQLException e) {
      LOGGER.log(Level.WARNING, "Main:insert " + e.getMessage());
    }
    catch (IllegalAccessException e) {
    }
    catch (IllegalArgumentException e) {
    }
    catch (InvocationTargetException e) {
    }
    finally {
      ConnectionFactory.close(insertStatement);
      ConnectionFactory.close(dbConnection);
    }
    return insertedId;
  }

  /**
   * Deletes a row with a given id from the table
   * 
   * @param id - id of the row to delete
   * @return int - (0) if deletion fails, (1) if it succeeds
   */
  public int delete(int id) {
    Connection dbConnection = ConnectionFactory.getConnection();
    PreparedStatement deleteStatement = null;
    int rs = 0;
    try {
      deleteStatement = dbConnection.prepareStatement(deleteStatementString);
      deleteStatement.setInt(1, id);
      rs = deleteStatement.executeUpdate();

    }
    catch (SQLException e) {
      LOGGER.log(Level.WARNING, "Main:delete " + e.getMessage());
    }
    finally {
      ConnectionFactory.close(deleteStatement);
      ConnectionFactory.close(dbConnection);
    }
    return rs;
  }

  /**
   * Updates the row with given id with data from the given object
   * 
   * @param id - id of the row to update
   * @param t - object to replace the row
   * @return int - (1) if update is successful, (0) if it fails
   */
  public int update(int id, T t) {
    Connection dbConnection = ConnectionFactory.getConnection();
    PreparedStatement updateStatement = null;
    int rs = 0;
    try {
      updateStatement = dbConnection.prepareStatement(updateStatementString);
      int insertNr = 2;
      for (Method m : methods) {
        if (isGetter(m) && !m.getName().equals("getClass")) {
          if (m.getName().equals("getId")) {
            updateStatement.setInt(1, (Integer) m.invoke(t));
            continue;
          }
          if (m.invoke(t).getClass() == Integer.class) {
            updateStatement.setInt(insertNr, (Integer) m.invoke(t));
          }
          else {
            updateStatement.setString(insertNr, (String) m.invoke(t));
          }
          insertNr++;
        }
      }
      updateStatement.setInt(insertNr, id);
      rs = updateStatement.executeUpdate();
    }
    catch (SQLException e) {
      LOGGER.log(Level.WARNING, "Main:update " + e.getMessage());
    }
    catch (IllegalAccessException e) {
      e.printStackTrace();
    }
    catch (IllegalArgumentException e) {
      e.printStackTrace();
    }
    catch (InvocationTargetException e) {
      e.printStackTrace();
    }
    finally {
      ConnectionFactory.close(updateStatement);
      ConnectionFactory.close(dbConnection);
    }
    return rs;
  }

  /**
   * Checks if a method is a getter
   * 
   * @param method
   * @return boolean - (true) when method is a getter, (false) when method is not a getter
   */
  private boolean isGetter(Method method) {
    if (!method.getName().startsWith("get"))
      return false;
    if (method.getParameterTypes().length != 0)
      return false;
    return true;
  }

  /**
   * Gets a sorted array of getters that have corresponding locations with the fields
   * 
   * @param fields - the fields of the Class
   * @param methods - the methods of the Class
   * @return Method[] - sorted array of getters, without getClass()
   */
  private Method[] getSortedGettersAfterFields(Field[] fields, Method[] methods) {
    ArrayList<Method> getters = new ArrayList<Method>();
    for (Method m : methods) {
      if (isGetter(m) && !m.getName().equals("getClass")) {
        getters.add(m);
      }
    }
    Method[] toRetrun = new Method[getters.size()];
    for (int i = 0; i < fields.length; i++) {
      for (Method m : getters) {
        if (fields[i].getName().equals(m.getName().substring(3, m.getName().length()).toLowerCase())) {
          toRetrun[i] = m;
          break;
        }
      }
    }
    return toRetrun;
  }
}
