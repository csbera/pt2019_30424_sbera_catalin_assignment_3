package presentation;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

import business.*;

public class View {
  JFrame f;
  JPanel tables;
  JPanel buttons;
  JTable Clients;
  JTable Products;
  JTable Stocks;
  JTable Orders;

  // Constructor
  public View() {
    f = new JFrame();
    tables = new JPanel();
    buttons = new JPanel();
    Clients = new JTable();
    Products = new JTable();
    Stocks = new JTable();
    Orders = new JTable();

    f.setTitle("Wharehouse database");
    f.setLayout(new GridLayout(2, 0));
    tables.setLayout(new GridLayout(1, 4));
    buttons.setLayout(new GridLayout(3, 1));

    updateTables();

    JScrollPane spClient = new JScrollPane(Clients);
    JScrollPane spProduct = new JScrollPane(Products);
    JScrollPane spStock = new JScrollPane(Stocks);
    JScrollPane spOrder = new JScrollPane(Orders);
    tables.add(spClient);
    tables.add(spProduct);
    tables.add(spStock);
    tables.add(spOrder);

    JPanel clientPanel = new JPanel();
    JLabel clientLabel = new JLabel("Client: ");
    clientPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    clientPanel.add(clientLabel);
    final JTextArea[] clientTextAreas = new JTextArea[3];
    clientTextAreas[0] = new JTextArea(1, 5);
    clientTextAreas[1] = new JTextArea(1, 40);
    clientTextAreas[2] = new JTextArea(1, 40);
    JButton updateClient = new JButton("Update");
    JButton insertClient = new JButton("Insert");
    JButton deleteClient = new JButton("Delete");
    clientPanel.add(new JLabel("Id: "));
    clientPanel.add(clientTextAreas[0]);
    clientPanel.add(new JLabel("Name: "));
    clientPanel.add(clientTextAreas[1]);
    clientPanel.add(new JLabel("Adress: "));
    clientPanel.add(clientTextAreas[2]);
    clientPanel.add(updateClient);
    clientPanel.add(insertClient);
    clientPanel.add(deleteClient);

    JPanel productPanel = new JPanel();
    JLabel productLabel = new JLabel("Product: ");
    productPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    productPanel.add(productLabel);
    final JTextArea[] productTextAreas = new JTextArea[3];
    productTextAreas[0] = new JTextArea(1, 5);
    productTextAreas[1] = new JTextArea(1, 50);
    productTextAreas[2] = new JTextArea(1, 5);
    JButton updateProduct = new JButton("Update");
    JButton insertProduct = new JButton("Insert");
    JButton deleteProduct = new JButton("Delete");
    productPanel.add(new JLabel("Id: "));
    productPanel.add(productTextAreas[0]);
    productPanel.add(new JLabel("Name: "));
    productPanel.add(productTextAreas[1]);
    productPanel.add(new JLabel("Amount: "));
    productPanel.add(productTextAreas[2]);
    productPanel.add(updateProduct);
    productPanel.add(insertProduct);
    productPanel.add(deleteProduct);

    JPanel orderPanel = new JPanel();
    JLabel orderLabel = new JLabel("Order: ");
    orderPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    orderPanel.add(orderLabel);
    final JTextArea[] orderTextAreas = new JTextArea[4];
    orderTextAreas[0] = new JTextArea(1, 20);
    orderTextAreas[1] = new JTextArea(1, 20);
    orderTextAreas[2] = new JTextArea(1, 20);
    orderTextAreas[3] = new JTextArea(1, 20);
    JButton createOrder = new JButton("Create Order");
    orderPanel.add(new JLabel("Id: "));
    orderPanel.add(orderTextAreas[0]);
    orderPanel.add(new JLabel("Client Id: "));
    orderPanel.add(orderTextAreas[1]);
    orderPanel.add(new JLabel("Product Id: "));
    orderPanel.add(orderTextAreas[2]);
    orderPanel.add(new JLabel("Amount: "));
    orderPanel.add(orderTextAreas[3]);
    orderPanel.add(createOrder);

    buttons.add(clientPanel);
    buttons.add(productPanel);
    buttons.add(orderPanel);

    f.add(tables);
    f.add(buttons);

    insertClient.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        ClientValidator cv = new ClientValidator();
        String[] parameters = new String[3];
        parameters[0] = clientTextAreas[0].getText();
        parameters[1] = clientTextAreas[1].getText();
        parameters[2] = clientTextAreas[2].getText();
        int status = cv.getStatus(parameters);
        if (status == 0) {
          cv.add(parameters);
          f.setTitle("Client added succesfuly");
        }
        else if (status == -1) {
          f.setTitle("Invalid client data");
        }
        else {
          f.setTitle("Client alredy in table");
        }
        updateTables();
      }
    });

    updateClient.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        ClientValidator cv = new ClientValidator();
        String[] parameters = new String[3];
        parameters[0] = clientTextAreas[0].getText();
        parameters[1] = clientTextAreas[1].getText();
        parameters[2] = clientTextAreas[2].getText();
        int status = cv.getStatus(parameters);
        if (status == 0) {
          f.setTitle("Client not int Table");
        }
        else if (status == -1) {
          f.setTitle("Invalid client data");
        }
        else {
          cv.update(parameters);
          f.setTitle("Client updated succesfuly");
        }
        updateTables();
      }
    });

    deleteClient.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        ClientValidator cv = new ClientValidator();
        String[] parameters = new String[3];
        parameters[0] = clientTextAreas[0].getText();
        parameters[1] = clientTextAreas[1].getText();
        parameters[2] = clientTextAreas[2].getText();
        int status = cv.getStatus(parameters);
        if (status == 0) {
          f.setTitle("Client not int Table");
        }
        else if (status == -1) {
          f.setTitle("Invalid client data");
        }
        else {
          if (cv.delete(parameters) == 0) {
            f.setTitle("SQL error detected");
          }
          else {
            f.setTitle("Client deleted succesfuly");
          }
        }
        updateTables();
      }
    });

    insertProduct.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        ProductValidator pv = new ProductValidator();
        StockValidator sv = new StockValidator();
        String[] pParameters = new String[2];
        String[] sParameters = new String[2];
        pParameters[0] = productTextAreas[0].getText();
        sParameters[0] = productTextAreas[0].getText();
        pParameters[1] = productTextAreas[1].getText();
        sParameters[1] = productTextAreas[2].getText();
        int pStatus = pv.getStatus(pParameters);
        if (pStatus == 0) {
          pv.add(pParameters);
        }
        else if (pStatus == -1) {
          f.setTitle("Invalid product data");
        }
        else {
          f.setTitle("Product alredy in table");
        }
        int sStatus = sv.getStatus(sParameters);
        if (sStatus == 0) {
          sv.add(sParameters);
          f.setTitle("Product added succesfuly");
        }
        else if (sStatus == -1) {
          f.setTitle("Invalid stock data");
          pv.delete(pParameters);
        }
        updateTables();
      }
    });

    deleteProduct.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        ProductValidator pv = new ProductValidator();
        StockValidator sv = new StockValidator();
        String[] pParameters = new String[2];
        String[] sParameters = new String[2];
        pParameters[0] = productTextAreas[0].getText();
        sParameters[0] = productTextAreas[0].getText();
        pParameters[1] = productTextAreas[1].getText();
        sParameters[1] = productTextAreas[2].getText();
        int pStatus = pv.getStatus(pParameters);
        int sStatus = sv.getStatus(sParameters);
        if (sStatus == 0 || pStatus == 0) {
          f.setTitle("Product not in table");
        }
        else if (sStatus == -1 || pStatus == -1) {
          f.setTitle("Invalid product data");
        }
        else {
          sv.delete(sParameters);
          if (pv.delete(pParameters) == 0) {
            f.setTitle("SQL error detected");
            sv.add(sParameters);
          }
          else {
            f.setTitle("Product deleted succesfuly");
          }
        }
        updateTables();
      }
    });

    updateProduct.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        ProductValidator pv = new ProductValidator();
        StockValidator sv = new StockValidator();
        String[] pParameters = new String[2];
        String[] sParameters = new String[2];
        pParameters[0] = productTextAreas[0].getText();
        sParameters[0] = productTextAreas[0].getText();
        pParameters[1] = productTextAreas[1].getText();
        sParameters[1] = productTextAreas[2].getText();
        int pStatus = pv.getStatus(pParameters);
        int sStatus = sv.getStatus(sParameters);
        if (pStatus == 0 && sStatus == 0) {
          f.setTitle("Product not in table");
        }
        else if (pStatus == -1 || sStatus == -1) {
          f.setTitle("Invalid product data");
        }
        else {
          sv.update(sParameters);
          pv.update(pParameters);
          f.setTitle("Product updated succesfuly");
        }
        updateTables();
      }
    });

    createOrder.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        OrderValidator ov = new OrderValidator();
        String[] parameters = new String[4];
        parameters[0] = orderTextAreas[0].getText();
        parameters[1] = orderTextAreas[1].getText();
        parameters[2] = orderTextAreas[2].getText();
        parameters[3] = orderTextAreas[3].getText();
        int status = ov.getStatus(parameters);
        if (status == 0) {
          ov.insert(parameters);
          f.setTitle("Order created succesfuly");
        }
        else if (status == -1) {
          f.setTitle("Invalid order data");
        }
        else if (status == -2) {
          f.setTitle("Not enough stock");
        }
        else {
          f.setTitle("Order alredy created");
        }
        updateTables();
      }
    });

    f.setSize(1500, 600);
    f.setVisible(true);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  /**
   * Gets the data from the database
   * 
   * @param business - object that connects to the database
   * @return DefaultTableModel - model with info from the database
   */
  private DefaultTableModel getTable(Business business) {
    String[][] data = business.getData();
    String[] fields = business.getFields();
    return new DefaultTableModel(data, fields);
  }

  /**
   * Updates all the data from the displayed JTables
   */
  private void updateTables() {

    final Business clientBusiness = BusinessFactory.getBusiness("Client");
    final Business productBusiness = BusinessFactory.getBusiness("Product");
    final Business stockBusiness = BusinessFactory.getBusiness("Stock");
    final Business orderBusiness = BusinessFactory.getBusiness("Orders");

    Clients.setModel(getTable(clientBusiness));
    Products.setModel(getTable(productBusiness));
    Stocks.setModel(getTable(stockBusiness));
    Orders.setModel(getTable(orderBusiness));
  }

  // Main method
  public static void main(String[] args) {
    new View();
  }
}
