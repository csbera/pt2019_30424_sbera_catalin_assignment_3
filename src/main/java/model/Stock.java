package model;

/**
 * Stud class that corresponds to the Stock Table
 * 
 * @author catal
 *
 */
public class Stock {
  private int id;
  private int amount;

  public Stock(int id, int amount) {
    super();
    this.id = id;
    this.amount = amount;
  }

  public Stock(String id, String amount) {
    this(Integer.parseInt(id), Integer.parseInt(amount));
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }

  @Override
  public String toString() {
    return "Stock [id=" + id + ", amount=" + amount + "]";
  }
}
