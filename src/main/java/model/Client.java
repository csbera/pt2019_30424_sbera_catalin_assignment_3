package model;

/**
 * Stud class that corresponds to the Client Table
 * 
 * @author catal
 *
 */
public class Client {

  private int id;
  private String name;
  private String adress;

  public Client(int id, String name, String adress) {

    this.name = name;
    this.id = id;
    this.adress = adress;
  }

  public Client(String id, String name, String adress) {
    this(Integer.parseInt(id), name, adress);
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAdress() {
    return adress;
  }

  public void setAdress(String adress) {
    this.adress = adress;
  }

  @Override
  public String toString() {
    return "Client [id=" + id + ", name=" + name + ", address=" + adress + "]";
  }
}
