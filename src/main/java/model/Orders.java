package model;

/**
 * Stud class that corresponds to the Orders Table
 * 
 * @author catal
 *
 */
public class Orders {
  private int id;
  private int idclient;
  private int idproduct;
  private int amount;

  public Orders(int id, int idClient, int idProduct, int amount) {
    super();
    this.id = id;
    this.idclient = idClient;
    this.idproduct = idProduct;
    this.amount = amount;
  }

  public Orders(String id, String idClient, String idProduct, String amount) {
    this(Integer.parseInt(id), Integer.parseInt(idClient), Integer.parseInt(idProduct), Integer.parseInt(amount));
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getIdClient() {
    return idclient;
  }

  public void setIdClient(int idClient) {
    this.idclient = idClient;
  }

  public int getIdProduct() {
    return idproduct;
  }

  public void setIdProduct(int idProduct) {
    this.idproduct = idProduct;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(int ammount) {
    this.amount = ammount;
  }

  @Override
  public String toString() {
    return "Orders [id=" + id + ", idClient=" + idclient + ", idProduct=" + idproduct + ", amount=" + amount + "]";
  }

}
